@extends('user.master')
@section('title', 'Đăng kí tài khoản')
@section('content')
<div id="register-content">
    @include('admin.blocks.error')
    <form action="" method="POST" style="width: 650px; margin: 30px auto;">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset>
        <legend>Thông Tin Đăng Kí</legend>                
        <table>
            <tr>
                <td class="login_img"></td>
                <td>
                    <span class="form_label">Username:</span>
                    <span class="form_item">
                        <input type="text" name="txtUser" class="textbox" />
                    </span><br />
                    <span class="form_label">Password:</span>
                    <span class="form_item">
                        <input type="password" name="txtPass" class="textbox" />
                    </span><br />
                    <span class="form_label">Repassword:</span>
                    <span class="form_item">
                        <input type="password" name="txtRepass" class="textbox" />
                    </span><br />
                    <span class="form_label">Image:</span>
                    <span class="form_item">
                        <input type="file" name="Image" class="" />
                    </span><br />
                    <span class="form_label"></span>
                    <span class="form_item">
                        <input type="submit" name="btnRegister" value="Đăng kí" class="button" />
                    </span>
                </td>
            </tr>
        </table>
    </fieldset>
    </form>
</div>
@endsection