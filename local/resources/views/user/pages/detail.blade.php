﻿@extends('user.master')
@section('title', 'Danh mục tin tức')
@section('content')
@foreach($news as $val)
<div class="news">
    <h1>{!! $val["title"] !!}</h1>
    <img src="{!! asset('local/public/uploads/news/'.$val['image']) !!}" class="thumbs" />
    <p>
        <i><b>Danh mục</b>: <a href="{!! url('the-loai/'.$val["category_id"].'/'.$val["cate"]["slug"]) !!}">{!! $val["cate"]["name"] !!}</a><br />
        <b>Nguồn</b>: VnExpress<br />
        <b>Viết bởi</b>: {!! $val["author"] !!}<br />
        <b>Ngày viết</b>: {!! $val["created_at"] !!}</i>
    </p>
    <p>{!! $val["intro"] !!}</p>
    <p>{!! $val["full"] !!}</p>
</div>  
@endforeach
@endsection