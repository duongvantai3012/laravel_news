@extends('user.master')
@section('title', 'Trang chủ')
@section('content')
@foreach($dataNews as $val)
<div class="news">
	<h1>{!! $val["title"] !!}</h1>
	<img src="{!! asset('local/public/uploads/news/'.$val['image']) !!}" class="thumbs" />
	<h3>Chuyên Mục: {!! $val["cate"]["name"] !!}</h3>
	<p>{!! $val["intro"] !!}</p>
	<a href="{!! url('chi-tiet-tin/'.$val["id"].'/'.$val["alias"].'.html') !!}" class="readmore">Đọc thêm</a>
	<div class="clearfix"></div>

</div>
@endforeach

<div id="pagin-home">
<ul>
  @if($dataNews->currentPage() != 1)
  <li><a href="{!! $dataNews->url($dataNews->currentPage()-1) !!}">Prev</a></li>
  @endif
  @for($i = 1; $i <= $dataNews->lastPage(); $i++ )
  <li class="{!! ($dataNews->currentPage() == $i ) ? 'active' : '' !!}">
    <a href="{!! $dataNews->url($i) !!}">{!! $i !!}</a>
  </li>
  @endfor
  @if($dataNews->currentPage() != $dataNews->lastPage())
  <li><a href="{!! $dataNews->url($dataNews->currentPage()+1) !!}">Next</a>
  </li>
  @endif
</ul>
</div>
@endsection
