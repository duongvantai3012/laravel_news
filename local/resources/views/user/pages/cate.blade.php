﻿@extends('user.master')
@section('title', 'Danh mục tin tức')
@section('content')


<h1 style="margin-bottom:10px;color:red">Thể Loại : {!! $cate["name"] !!}</h1>

@foreach($dataNews as $val)
<div class="news">
    <h1>{!! $val["title"] !!}</h1>
    <img src="{!! asset('local/public/uploads/news/'.$val['image']) !!}" class="thumbs" />
    <p>{!! $val["intro"] !!}</p>
    <a href="{!! url('chi-tiet-tin/'.$val["id"].'/'.$val["alias"].'.html') !!}" class="readmore">Đọc thêm</a>
    <div class="clearfix"></div>
</div>

@endforeach

@foreach($page as $value)

<div class="news">
	<h1></h1>	
</div>

@endforeach

{{-- {!! $page->links() !!} --}}

@endsection