﻿<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="QuocTuan.Info" />
    <link rel="stylesheet" href="templates/css/style.css" />
    <link rel="stylesheet" href="{!! asset('local/public/user/templates/css/style.css') !!}" />
    <title>@yield('title')</title>
</head>
<body>
    <div id="layout">
        <div id="top">
            Banner
        </div>
        <div id="topmenu">
            <ul>
                <li><a href="{!! url('/') !!}">Trang Chủ</a></li>
                <?php $data = App\Models\Cate::select('id', 'name', 'parent_id', 'slug')->where('parent_id', 0)->get()->toArray();?>
                @foreach($data as $menu_level_1)
                <li><a href="">{!! $menu_level_1["name"] !!}</a>
                    <ul>
                        <?php $data2 = App\Models\Cate::select('id', 'name', 'parent_id', 'slug')->where('parent_id',$menu_level_1["id"])->get()->toArray();?>
                        @foreach($data2 as $menu_level_2)
                        <li><a href="{!! route('getCate', ['id' => $menu_level_2["id"], 'alias' => $menu_level_2["slug"]]) !!}">{!! $menu_level_2["name"] !!}</a></li>
                        @endforeach
                    </ul>
                </li>
                @endforeach



                
         
            </ul>
        </div>
        <div id="content">
            <div id="left">
                <div id="leftmenu">
          
                </div>
            </div>
            <div id="main">
				@yield('content')           
			</div>
            <div class="clearfix"></div>
        </div>
        <div id="bottom">
            Copyright &copy; 2016
        </div>
    </div>
</body>
</html>