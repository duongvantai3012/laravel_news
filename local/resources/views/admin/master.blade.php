<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="QuocTuan.Info" />
    <link rel="stylesheet" href="{!! asset('local/public/admin/templates/css/style.css') !!}" />
	<title>@yield('title')</title>
	<script type="text/javascript" src="{!! asset('local/public/admin/templates/js/plugin/ckeditor/ckeditor.js') !!}"></script>
</head>

<body>
<div id="layout">
    <div id="top">
        Admin Area :: Trang chính
    </div>
	<div id="menu">
		<table width="100%">
			<tr>
				<td>
					<a href="{!! url('/admin') !!}">Trang chính</a> | <a href="{!! route('getUserList') !!}">Quản lý user</a> | <a href="{!! route('getCateList') !!}">Quản lý danh mục</a> | <a href="{!! route('getNewsList') !!}">Quản lý tin</a>
				</td>
				<td align="right">
					Xin chào {!! Auth::user()->username !!} | <a href="{!! route('getLogout') !!}">Logout</a>
				</td>
			</tr>
		</table>
	</div>
    <div id="main">
    	@include('admin.blocks.flash')
    	@include('admin.blocks.error')
		@yield('content')
	</div>
    <div id="bottom">
        Copyright © 2016 
    </div>
</div>
<script type="text/javascript" src="{!! asset('local/public/admin/templates/js/jquery-1.11.3.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('local/public/admin/templates/js/myscript.js') !!}"></script>
</body>
</html>