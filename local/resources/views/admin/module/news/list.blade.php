@extends('admin.master')
@section('title','Danh sách tin tức')
@section('content')
<table class="list_table">
    <tr class="list_heading">
        <td class="id_col">STT</td>
        <td>Tiêu Đề</td>
        <td>Tác Giả</td>
        <td>Thời Gian</td>
        <td class="action_col">Quản lý?</td>
    </tr>
    <?php 
    $stt = 0; ?>
    @foreach($dataNews as $val)
    <?php $stt++;?>
    <tr class="list_data">
        <td class="aligncenter">{!! $stt !!}</td>
        <td class="list_td aligncenter">{!! $val["title"] !!}</td>
        <td class="list_td aligncenter">{!! $val["author"] !!}</td>
        <td class="list_td aligncenter">
        <?php \Carbon\Carbon::setlocale('vi') ;?>
        {!! \Carbon\Carbon::createFromTimeStamp(strtotime($val["created_at"]))->diffForHumans() !!}
        </td>
        <td class="list_td aligncenter">
            <a href="{!! route('getNewsEdit', ['id' => $val['id']]) !!}"><img src="{!! asset('local/public/admin/images/edit.png') !!}" /></a>&nbsp;&nbsp;&nbsp;
            <a href="{!! route('getNewsDel', ['id' => $val['id']]) !!}" onclick="return xacnhanxoa('Bạn có chắc muốn xóa tin tức này?')"><img src="{!! asset('local/public/admin/images/delete.png') !!}" /></a>
        </td>
    </tr>
    @endforeach
</table>
@endsection