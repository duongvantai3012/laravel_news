@extends('admin.master')
@section('title','Danh Sách Thành Viên')
@section('content')
<table class="list_table">
    <tr class="list_heading">
        <td class="id_col">STT</td>
        <td>Username</td>
        <td>Level</td>
        <td class="action_col">Quản lý?</td>
    </tr>
    <?php $stt=0;?>
    @foreach($user as $val)
    <?php $stt++;?>
    <tr class="list_data">
        <td class="aligncenter">{!! $stt !!}</td>
        <td class="list_td aligncenter">{!! $val["username"] !!}</td>
        <td class="list_td aligncenter">
            @if($val["level"] == 1 && $val["id"] == 1)
            <span style="color: red; font-weight: bold;">Super Admin</span>
            @elseif($val["level"] == 1)
            <span style="color: blue; font-weight: bold;">Admin</span>
            @else
            <span style="color: black;">Member</span>
            @endif
        </td>
        <td class="list_td aligncenter">
            <a href="{!! route('getUserEdit', ['id' => $val['id']]) !!}"><img src="{!! asset('local/public/admin/images/edit.png') !!}" /></a>&nbsp;&nbsp;&nbsp;
            <a href="{!! route('getUserDel', ['id' => $val['id']]) !!}"><img src="{!! asset('local/public/admin/images/delete.png') !!}" onclick="return xacnhanxoa('Bạn có chắc muốn xóa thành viên này?')"/></a>
        </td>
    </tr>
    @endforeach
</table>
@endsection