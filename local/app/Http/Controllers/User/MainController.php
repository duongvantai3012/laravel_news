<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cate;
use App\Models\News;
use App\Models\User;
use App\Http\Requests\RegisterRequest;
use DateTime;

class MainController extends Controller
{
   	
	public function getIndex() {
      $dataNews = News::with('cate')->select('id', 'title','alias', 'intro', 'image', 'category_id')->orderBy('id', 'DESC')->limit(6)->paginate(4);
		return view('user.pages.index', ['dataNews' => $dataNews]);
	}

	public function getCate($id) {
      $cate = Cate::select('id', 'name')->where('id', $id)->first()->toArray();
      $dataNews = News::with('cate')->select('id', 'title', 'alias', 'intro', 'image', 'category_id')->where('category_id', $id)->get()->toArray();
      $page = News::paginate(1);
		return view('user.pages.cate', ['cate' => $cate, 'dataNews' => $dataNews, 'page'=>$page]);
	}

	public function getDetail($id) {
      $news = News::with('cate')->where('id', $id)->orderBy('id', 'DESC')->get()->toArray();
		return view('user.pages.detail', ['news' => $news]);
	}
}
