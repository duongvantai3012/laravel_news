<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsAddRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sltCate' => 'required',
            'txtTitle' => 'required|unique:news,title',
            'txtAuthor' => 'required',
            'txtIntro' => 'required',
            'txtFull' => 'required',
            'newsImg' => 'required|image|mimes:jpeg,bmp,png,jpg',
        ];
    }

    public function messages() {
        return [
            'sltCate.required' => 'Vui Lòng Chọn Danh Mục',
            'txtTitle.required' => 'Vui Lòng Chọn Tiêu Đề',
            'txtTitle.unique' => 'Tiêu Đề Đã Tồn Tại',
            'txtAuthor.required' => 'Vui Lòng Chọn Tác Giả',
            'txtIntro.required' => 'Vui Lòng Giới Thiệu',
            'txtFull.required' => 'Vui Lòng Nhập Nội Dung',
            'newsImg.required' => 'Vui Lòng Chọn Hình Ảnh',
            'newsImg.image' => 'Vui Lòng Chọn Định Dạng Hình',
            'newsImg.mimes' => 'Hình Bạn Chọn Phải Có Định Dạng Sau jpeg,bmp,png,jpg'
        ];
    }
}
