<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserEditRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtPass' => 'required',
            'txtRepass' => 'required|same:txtPass',
        ];
    }

    public function messages() {
        return [
            'txtPass.required' => 'Vui Lòng Nhập Mật Khẩu',
            'txtRepass.required' => 'Vui Lòng Nhập Lại Mật Khẩu',
            'txtRepass.same' => 'Hai Mật Khẩu Không Trùng Nhau',
        ];
    }
}
