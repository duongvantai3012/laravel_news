<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtUser' => 'required|unique:users,username',
            'txtPass' => 'required',
            'txtRepass' => 'required|same:txtPass',
            'Image' => 'required|image|mimes:jpeg,bmp,png,jpg',
        ];
    }

    public function messages() {
        return [
            'txtUser.required' => 'Vui Lòng Nhập Tài Khoản',
            'txtUser.unique' => 'Tài Khoản Đã Tồn Tại',
            'txtPass.required' => 'Vui Lòng Nhập Mật Khẩu',
            'txtRepass.required' => 'Vui Lòng Nhập Lại Mật Khẩu',
            'txtRepass.same' => 'Hai Mật Khẩu Không Trùng Nhau',
            'Image.required' => 'Vui lòng chọn hình ảnh',
            'Image.image' => 'Vui lòng chọn định dạng hình ảnh',
            'Image.mimes' => 'Hình phải trong các định dạng sau : jpeg,bmp,png,jpg',
        ];
    }
}
