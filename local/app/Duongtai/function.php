<?php 
function menuMulti($data, $parent_id=0, $str="---|", $select=0) {
	foreach($data as $val){
		$id = $val["id"];
		$name = $val["name"];
		if ($val["parent_id"] == $parent_id){
			if( $select != 0 && $id == $select){
				echo '<option value="'.$id.'" selected>'.$str.' '.$name.'</option>';
			}else{
				echo '<option value="'.$id.'">'.$str.' '.$name.'</option>';
			}
			menuMulti($data, $id, $str."---|",$select);
		}
		
	}
}

function listMulti($data, $parent_id=0, $str="") {
	foreach($data as $val) {
		$id = $val["id"];
		$name = $val["name"];
		if ( $val["parent_id"] == $parent_id){
			echo '
			<tr class="list_data">';
		        if($val["parent_id"] == 0){
		        	echo '<td class="list_td alignleft"><b>'.$str.' '.$name.'</b></td>';
		        }else{
		        	echo '<td class="list_td alignleft">'.$str.' '.$name.'</td>';
		        }
		        echo'
		        <td class="list_td aligncenter">
		            <a href="edit/'.$id.'"><img src="'. asset('local/public/admin/images/edit.png') .'" /></a>&nbsp;&nbsp;&nbsp;
		            <a href="delete/'.$id.'" onclick="return xacnhanxoa(\'Bạn có chắc muốn xóa danh mục này?\')"><img src="'. asset('local/public/admin/images/delete.png') .'" /></a>
		        </td>
		    </tr>';
		    listMulti($data, $id, $str."---|");
		}
		
	}
}

function subMenu($data, $id) {
	echo '<ul>';
	foreach($data as $val){
		if ($val["parent_id"] == $id){
			echo '<li><a href="../../the-loai/'.$val["id"].'/'.$val["slug"].'">'.$val["name"].'</a>';
			subMenu($data, $val["id"]);
			echo '</li>';
		}
	}
	echo '</ul>';
}

?>